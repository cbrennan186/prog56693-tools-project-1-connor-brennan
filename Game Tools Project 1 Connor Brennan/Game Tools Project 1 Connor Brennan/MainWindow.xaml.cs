﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Game_Tools_Project_1_Connor_Brennan
{
    /// <summary>
    /// The code that runs in the background for the WPF form to run
    /// </summary>
    public partial class MainWindow : Window
    {
        //Variuable declaration
        string fileToWritePath;
        string fileToReadPath;
        string SelectedFile;
        string DataPath;

        /// <summary>
        /// Code that runs on start and not again. Adds the choices in the combo box, 
        /// initializes the components, and sets the data pathway to where the data files are located. 
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            FileSelector.Items.Add("EnemyStartData");
            FileSelector.Items.Add("LargeMeteorData");
            FileSelector.Items.Add("PlayerData");
            FileSelector.Items.Add("SmallMeteorData");
            FileSelector.Items.Add("WindowData");

            DataPath = @"C:\Users\cbren\Desktop\Game Programs\Game Tools Project 1\Game Tools Project 1 Connor Brennan\Game Tools Project 1 Connor Brennan\Assets\";
        }

        /// <summary>
        /// When a selection is choosen in the combobox, load in the related json files data 
        /// //and put it into the textbox for the user to edit. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FileSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedFile = FileSelector.SelectedValue.ToString();
            if (SelectedFile == "EnemyStartData")
            {
                fileToReadPath = System.IO.File.ReadAllText(DataPath + "EnemyStartData.json");
                DataText.Text = fileToReadPath;
            }
            else if (SelectedFile == "LargeMeteorData")
            {
                fileToReadPath = System.IO.File.ReadAllText(DataPath + "LargeMeteorData.json");
                DataText.Text = fileToReadPath;
            }
            else if (SelectedFile == "PlayerData")
            {
                fileToReadPath = System.IO.File.ReadAllText(DataPath + "PlayerData.json");
                DataText.Text = fileToReadPath;
            }
            else if (SelectedFile == "SmallMeteorData")
            {
                fileToReadPath = System.IO.File.ReadAllText(DataPath + "SmallMeteorData.json");
                DataText.Text = fileToReadPath;
            }
            else if (SelectedFile == "WindowData")
            {
                fileToReadPath = System.IO.File.ReadAllText(DataPath + "WindowData.json");
                DataText.Text = fileToReadPath;
            }
            else
            {
                DataText.Text = "That was not a valid file";
            }
        }

        /// <summary>
        /// When the button is clicked, take the data in the textbox and write it to the 
        /// selected json file. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            fileToWritePath = DataText.Text;

            if (SelectedFile == "EnemyStartData")
            {
                System.IO.File.WriteAllText(DataPath+"EnemyStartData.json", fileToWritePath);
            }
            else if (SelectedFile == "LargeMeteorData")
            {
                System.IO.File.WriteAllText(DataPath+"LargeMeteorData.json", fileToWritePath);
            }
            else if (SelectedFile == "PlayerData")
            {
                System.IO.File.WriteAllText(DataPath+"PlayerData.json", fileToWritePath);
            }
            else if (SelectedFile == "SmallMeteorData")
            {
                System.IO.File.WriteAllText(DataPath+"SmallMeteorData.json", fileToWritePath);
            }
            else if (SelectedFile == "WindowData")
            {
                System.IO.File.WriteAllText(DataPath+"WindowData.json", fileToWritePath);
            }
            else
            {
                DataText.Text = "That was not a valid file";
            }
        }
    }
}
